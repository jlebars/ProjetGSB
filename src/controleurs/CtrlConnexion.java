package controleurs;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vues.VueConnexion;
import modele.dao.DaoConnexion;
import modele.dao.Jdbc;
import modele.metier.Visiteur;


public class CtrlConnexion implements WindowListener, ActionListener, KeyListener, FocusListener {

    /**
     * La vue connexion
     */
    private VueConnexion vue; // LA VUE
    private final CtrlPrincipal ctrlPrincipal;
    private boolean isConnected = false;
    private boolean loginFocused = false;
    private boolean mdpFocused = false;
 

    /**
     * Constructeur du controlleur connexion
     *
     * @param vue
     * @param ctrl
     */
    public CtrlConnexion(final VueConnexion vue, CtrlPrincipal ctrl) {
        this.vue = vue;
        this.ctrlPrincipal = ctrl;

        this.vue.addWindowListener(this);
        // ajout des boutons de la vue au listener
        enable();
        vue.getjButtonOk().addActionListener(this);
        vue.getjTextFieldLogin().addKeyListener(this);
        vue.getjPasswordMdp().addKeyListener(this);
    }

    /**
     *
     */
    public void connecter() {
        loading();
        try {
            Jdbc.getInstance().connecter();
            isConnected = true;
            enable();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(CtrlConnexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        stopLoading();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource().equals(vue.getjTextFieldLogin())) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                connection();
            }
        }
        if (e.getSource().equals(vue.getjPasswordMdp())) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                connection();
            }
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        if (e.getSource().equals(vue.getjTextFieldLogin())) {
            loginFocused = true;
        }
        if (e.getSource().equals(vue.getjPasswordMdp())) {
            mdpFocused = true;
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (e.getSource().equals(vue.getjTextFieldLogin())) {
            loginFocused = false;
        }
        if (e.getSource().equals(vue.getjPasswordMdp())) {
            mdpFocused = false;
        }
    }

    private void loading() {
        vue.getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    /**
     *
     */
    public void stopLoading() {
        vue.getContentPane().setCursor(null);
    }

    private void enable() {
        vue.getjButtonOk().setEnabled(isConnected);
        vue.getjTextFieldLogin().setEnabled(isConnected);
        vue.getjPasswordMdp().setEnabled(isConnected);
    }

    @Override
    public void actionPerformed(ActionEvent evenement) {
        if (isConnected) {
            if (evenement.getSource() == vue.getjButtonOk()) {
                connection();
            }
        }
    }


    /**
     * Tentative de connection
     */
    private void connection() {
        String login = vue.getjTextFieldLogin().getText().replaceAll("\\s{2,}", "").trim();
        String mdp = vue.getjPasswordMdp().getText().replaceAll("\\s{2,}", "").trim();
        if (mdp.equals("") && login.equals("")) {
            JOptionPane.showMessageDialog(vue, "Veuillez spécifier votre login et votre mot de passe.");
            vue.getjTextFieldLogin().requestFocus();
            vue.getjTextFieldLogin().selectAll();
        } else if (mdp.equals("")) {
            if (!loginFocused) {
                JOptionPane.showMessageDialog(vue, "Veuillez spécifier votre mot de passe.");
            }
            vue.getjPasswordMdp().requestFocus();
            vue.getjPasswordMdp().selectAll();
        } else if (login.equals("")) {
            JOptionPane.showMessageDialog(vue, "Veuillez spécifier votre login.");
            vue.getjTextFieldLogin().requestFocus();
            vue.getjTextFieldLogin().selectAll();
        } else {
            try {
                //ouvre le menu général si les données sont correctes
                int code = DaoConnexion.verifierInfosConnexion(login, mdp);
                if (code == 11) {//bon login et mdp
                    openApp();
                } else if (code == 0) {//mauvais login et mdp
                    JOptionPane.showMessageDialog(vue, "Votre login et votre mot de passe ne sont pas corrects.");
                    vue.getjTextFieldLogin().requestFocus();
                    vue.getjTextFieldLogin().selectAll();
                } else if (code == 1) {//mauvais login bon mdp
                    JOptionPane.showMessageDialog(vue, "Votre login est incorrect.");
                    vue.getjTextFieldLogin().requestFocus();
                    vue.getjTextFieldLogin().selectAll();
                } else if (code == 10) {//bon login mauvais mdp
                    JOptionPane.showMessageDialog(vue, "Votre mot de passe est incorrect.");
                    vue.getjPasswordMdp().requestFocus();
                    vue.getjPasswordMdp().selectAll();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(vue, ex, "Erreur de communication avec la base de donnée. ", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     *
     */
    public void openApp() {
        try {
            Visiteur leVisiteur = DaoConnexion.getConnectedVisiteur(vue.getjTextFieldLogin().getText(), vue.getjPasswordMdp().getText());
            try {
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("visiteur.data"));
                oos.writeObject(leVisiteur);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(vue, ex, "Impossible de sauvegarder l'état de l'objet visiteur.", JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(CtrlConnexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(vue, ex, "Erreur SQL ", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(CtrlConnexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        // afficher la vue
        ctrlPrincipal.afficherMenuGeneral(getVue());
    }

    // ACCESSEURS et MUTATEURS
    /**
     *
     * @return vue
     */
    public VueConnexion getVue() {
        return vue;
    }

    /**
     *
     * @param vue
     */
    public void setVue(VueConnexion vue) {
        this.vue = vue;
    }

    // REACTIONS EVENEMENTIELLES
    /**
     *
     * @param e
     */
    @Override
    public void windowOpened(WindowEvent e) {
    }

    /**
     *
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        ctrlPrincipal.quitterApplication();
    }

    /**
     *
     * @param e
     */
    @Override
    public void windowClosed(WindowEvent e) {
    }

    /**
     *
     * @param e
     */
    @Override
    public void windowIconified(WindowEvent e) {
    }

    /**
     *
     * @param e
     */
    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    /**
     *
     * @param e
     */
    @Override
    public void windowActivated(WindowEvent e) {
    }

    /**
     *
     * @param e
     */
    @Override
    public void windowDeactivated(WindowEvent e) {
    }
    /**
     * 
     * @param e 
     */
    @Override
    public void keyReleased(KeyEvent e) {
    }
}
