/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modele.metier.Visiteur;


public class FileReader {

    /**
     *
     * @param laVue
     * @return
     */
    public static Visiteur getConnectedVisiteur(JFrame laVue) {
        Visiteur visiteurLu = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("visiteur.data"));
            visiteurLu = (Visiteur) ois.readObject();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(laVue, "Impossible de lire le fichier contenant l'état de l'objet visiteur." + ex, "Erreur lecture", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(laVue, "Erreur impossible de charger le fichier contenant l'état de l'objet du visiteur\n" + ex, "Erreur chargement", JOptionPane.ERROR_MESSAGE);
        }
        return visiteurLu;
    }

    private void close(ObjectInputStream ois, JFrame laVue) {
        try {
            if (ois != null) {
                ois.close();
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(laVue, "Erreur impossible de fermer le fichier contenant l'état de l'objet du visiteur\n" + ex, "Erreur fermeture", JOptionPane.ERROR_MESSAGE);
        }
    }
}
